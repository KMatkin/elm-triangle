module Main exposing (..)

import AnimationFrame
import Html exposing (Html, div, input)
import Html.Attributes exposing (width, height, style, type_, min, max, value)
import Html.Events exposing (onInput)
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector3 as Vec3 exposing (vec3, Vec3)
import Math.Vector2 as Vec2 exposing (vec2, Vec2)
import WebGL exposing (Mesh, Shader)
import Time exposing (Time)
import String exposing (toInt)

type alias Model = 
    { depth: Int
    , time: Float
    }

type Msg = ChangeDepth Int | Tick Float

init : (Model, Cmd Msg)
init = 
    (Model 0 0, Cmd.none)

main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , subscriptions = (\model -> AnimationFrame.diffs Tick)
        , update = update
        }

---- UPDATE ----

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick t -> ({ model | time = t + .time model }, Cmd.none)
        ChangeDepth v -> ({ model | depth = v }, Cmd.none)


---- VIEW ----

view : Model -> Html Msg
view model =
    let
        canvas = WebGL.toHtml
            [ width 600
            , height 600
            , style [ ( "display", "block" ) ]
            ]
            [ WebGL.entity 
                vertexShader
                fragmentShader
                (mesh (.depth model))
                { perspective = perspective (.time model / 1000) }
            ]
        slider = input [ type_ "range"
        , Html.Attributes.min "0"
        , Html.Attributes.max "9"
        , Html.Attributes.value <| toString <| .depth model
        , onInput (\v -> toInt v |> Result.withDefault 0 |> ChangeDepth)
        ] []
    in   
        div []
        [ slider
        , canvas
        ]
    

perspective : Float -> Mat4
perspective t =
    Mat4.mul
        (Mat4.makePerspective 45 1 0.01 100)
        (Mat4.makeLookAt (vec3 (4 * cos t) 0 (4 * sin t)) (vec3 0 0 0) (vec3 0 1 0))

-- Mesh

type alias Vertex =
    { position : Vec3
    }

type alias Triangle =
    { a: Vertex
    , b: Vertex
    , c: Vertex
    }

vec2ToVec3 : Vec2 -> Vec3
vec2ToVec3 a = vec3 (Vec2.getX a) (Vec2.getY a) 0

newTriangle : Vec2 -> Vec2 -> Vec2 -> Triangle
newTriangle a b c =
        Triangle (Vertex (vec2ToVec3 a))
                 (Vertex (vec2ToVec3 b))
                 (Vertex (vec2ToVec3 c))

triangleToVertices : Triangle -> (Vertex, Vertex, Vertex)
triangleToVertices t =  (t.a, t.b, t.c)

initialTriangle : Triangle
initialTriangle = Triangle (Vertex (vec3 0 1 0)) (Vertex(vec3 1 0 0)) (Vertex(vec3 -1 0 0))

middle : Vec3 -> Vec3 -> Vec3
middle a b = Vec3.add a b |> Vec3.scale 0.5

splitTriangle : Triangle -> List Triangle
splitTriangle t = 
    let
        a = .position (.a t)
        b = .position (.b t)
        c = .position (.c t)
        middleAB = middle a b
        middleBC = middle b c
        middleAC = middle a c
        triangle1 = Triangle (Vertex a) (Vertex middleAB) (Vertex middleAC)
        triangle2 = Triangle (Vertex middleAB) (Vertex b) (Vertex middleBC)
        triangle3 = Triangle (Vertex middleAC) (Vertex middleBC) (Vertex c)
    in
        [triangle1, triangle2, triangle3]

recSplit : Int -> List Triangle -> List Triangle
recSplit depth l = 
    case depth of
    0 -> l
    _ -> recSplit (depth - 1) (List.concat (List.map splitTriangle l))

mesh : Int -> Mesh Vertex
mesh depth =
    recSplit depth [initialTriangle]
    |> List.map triangleToVertices
    |> WebGL.triangles

-- Shaders

type alias Uniforms =
    { perspective : Mat4 }


vertexShader : Shader Vertex Uniforms { vcolor : Vec3 }
vertexShader =
    [glsl|
        attribute vec3 position;
        uniform mat4 perspective;
        varying vec3 vcolor;
        void main () {
            gl_Position = perspective * vec4(position, 1.0);
            vcolor = vec3(0.0);
        }
    |]


fragmentShader : Shader {} Uniforms { vcolor : Vec3 }
fragmentShader =
    [glsl|
        precision mediump float;
        varying vec3 vcolor;
        void main () {
            gl_FragColor = vec4(vcolor, 1.0);
        }
    |]